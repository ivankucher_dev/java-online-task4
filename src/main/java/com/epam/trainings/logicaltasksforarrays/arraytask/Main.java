package com.epam.trainings.logicaltasksforarrays.arraytask;

import com.epam.trainings.logicaltasksforarrays.arraytask.anothertwotasks.ArrayWorker;
import com.epam.trainings.logicaltasksforarrays.arraytask.joinsubtask.ArrayJoinWorker;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        //Uncomment to test tasks

        //test for anotherTwoTasks
       Integer arrayForDeleteDuplicatedTest[] = {0,1,2,2,4,4,4,5,5,6,6,6,8,9,9,9,10};
        Integer trackDuplicatedSeries[] = {0,0,0,1,2,3,0,4,4,7,4,4,4,4,4,4,4,4,4,4,4,1,1,1,1,1,2,3,5};
        ArrayWorker<Integer> arrayWorker = new ArrayWorker<>(arrayForDeleteDuplicatedTest);
        ArrayWorker<Integer> arrayWorker1 = new ArrayWorker<>(trackDuplicatedSeries);
        arrayWorker.deleteDuplicated();
        arrayWorker1.trackSeriesOfDuplicated();


        //test for joinTask
        Integer firstArray[]={0,1,4,6,5,11,18,19,22,26};
        Integer secondarray[]={0,1,4,6,5,11,18,19,22,24,25,26};
        //for right or left join
        ArrayJoinWorker<Integer> arrayJoinWorker = new ArrayJoinWorker<>(secondarray,firstArray);
        //for inner join
        ArrayJoinWorker<Integer> arrayJoinWorker1 = new ArrayJoinWorker<>(firstArray,secondarray);
arrayJoinWorker.leftOrRightJoin();
arrayJoinWorker1.innerJoin();

    }
}
