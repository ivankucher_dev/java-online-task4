package com.epam.trainings.logicaltasksforarrays.arraytask.anothertwotasks;

public interface ArrayFunctions<T> {

    public T[] deleteDuplicated();
    public T[] trackSeriesOfDuplicated();

}
