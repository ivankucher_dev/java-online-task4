package com.epam.trainings.logicaltasksforarrays.arraytask.anothertwotasks;


import java.util.Arrays;
/*
 * Видалити в масиві всі числа, які повторюються більше двох разів.
 * */
    public class ArrayWorker<T> implements ArrayFunctions<T> {

        private T[] array1;
        private Object[] finalArray;

        public ArrayWorker(T[] firstArray) {
            this.array1 = firstArray;


        }

        public T[] getFinalArray() {
            return (T[]) finalArray;
        }


        @Override
        public T[] deleteDuplicated() {
            finalArray = new Object[0];
            Arrays.sort(array1);
            for (int i = 0; i < array1.length; i++) {
                if (i == (array1.length - 1)) {
                    resize();
                    finalArray[finalArray.length - 1] = array1[i];
                } else if (array1[i] == array1[i + 1] && array1[i] == array1[i + 2]) {
                    i +=2;
                } else {
                    resize();
                    finalArray[finalArray.length - 1] = array1[i];
                }
            }
                for (T k : (T[]) finalArray) {
                    System.out.println(k);
                }
                return (T[]) finalArray;

        }

    @Override
    public T[] trackSeriesOfDuplicated() {
        finalArray = new Object[0];
        for(int i=0; i< array1.length;i++){
            if (i == (array1.length - 1)) {
                resize();
                finalArray[finalArray.length - 1] = array1[i];
            }
          else if (array1[i] == array1[i + 1]) {
                resize();
                finalArray[finalArray.length - 1] = array1[i];
                while(array1[i] == array1[i + 1]){
                    i++;
                }
            }else{
                resize();
                finalArray[finalArray.length - 1] = array1[i];
            }
        }
        for (T k : (T[]) finalArray) {
            System.out.println(k);
        }
        return  (T[]) finalArray;
    }


    public void resize() {
            finalArray = Arrays.copyOf(finalArray, finalArray.length + 1);
        }

        public boolean contain(Object[] arr, T value) {
            for (T i : (T[]) arr) {
                if (i == value) {
                    return true;
                }
            }
            return false;
        }



    }


