package com.epam.trainings.logicaltasksforarrays.arraytask.joinsubtask;

public interface ArrayFunctions<T> {

    public T[] innerJoin();
    public T[] leftOrRightJoin();



    }

