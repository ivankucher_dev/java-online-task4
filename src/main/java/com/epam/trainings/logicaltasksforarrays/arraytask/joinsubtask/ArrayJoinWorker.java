package com.epam.trainings.logicaltasksforarrays.arraytask.joinsubtask;

import java.util.Arrays;
/*
* Дано два масиви. Сформувати третій масив, що складається з тих елементів,
які: а) присутні в обох масивах; б) присутні тільки в одному з масивів.
* */
public class ArrayJoinWorker<T> implements ArrayFunctions<T> {

    private T[] array1;
    private T[] array2;
    private Object[] finalArray;

   public ArrayJoinWorker(T[] firstArray, T[] secondArray) {
        this.array1 = firstArray;
        this.array2 = secondArray;

    }

    public T[] getFinalArray() {
        return (T[]) finalArray;
    }

    @Override
    public T[] innerJoin() {
        finalArray = new Object[0];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    if (!contain(finalArray, array1[i])) {
                        resize();
                        finalArray[finalArray.length - 1] = array1[i];
                    }
                }
            }
        }

        for (T k : (T[]) finalArray) {
            System.out.println(k);
        }
        return (T[]) finalArray;
    }

    @Override
    public T[] leftOrRightJoin() {
        finalArray = new Object[0];
        int counter;
        for (int i = 0; i < array1.length; i++) {
            counter = 0;
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    counter++;
                }
                if (j == (array2.length - 1) && counter == 0) {
                    if (!contain(finalArray, array1[i])) {
                        resize();
                        finalArray[finalArray.length - 1] = array1[i];
                    }
                }
            }
        }

        for (T k : (T[]) finalArray) {
            System.out.println(k);
        }
        return (T[]) finalArray;
    }



    public void resize() {
        finalArray = Arrays.copyOf(finalArray, finalArray.length + 1);
    }

    public boolean contain(Object[] arr, T value) {
        for (T i : (T[]) arr) {
            if (i == value) {
                return true;
            }
        }
        return false;
    }


}
