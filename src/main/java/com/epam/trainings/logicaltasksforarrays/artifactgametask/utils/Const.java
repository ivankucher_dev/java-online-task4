package com.epam.trainings.logicaltasksforarrays.artifactgametask.utils;

public class Const {
    public static final int MIN_DAMAGE_FOR_ENEMY = 5;
    public static final int MAX_DAMAGE_FOR_ENEMY = 100;
    public static final int MIN_INCREASE_BY_ARTIFACT = 10;
    public static final int MAX_INCREASE_BY_ARTIFACT = 80;
    public static final String ENEMY = "enemy";
    public static final boolean PLAYER_STILL_ALIVE = true;
    public static final boolean PLAYER_IS_DEAD = false;
    public static final String ARTIFACT = "artifact";
    public static final int DOORS_AMOUNT = 10;
}
