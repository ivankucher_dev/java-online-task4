package com.epam.trainings.logicaltasksforarrays.artifactgametask;

import com.epam.trainings.collectionstasks.secondtask.Menu;

public class Main {
    public static void main(String[] args) {
        boolean mode;
        System.out.println("_________________WELCOME TO ARTIFACT GAME___________________");
        System.out.println("You got a player with 25 hp and 10 doors around you , open one and try your luck.");
        System.out.println("+@+ Behind the door you can find artifact , that give you from 10 to 80 hp");
        System.out.println("+@+ Or behind the door could be a monster , that dicrease your hp from 5 to 100");
        System.out.println("Developed by Ivan Kucher");
        System.out.println("_________________PRESS 1 TO START BATTLE____________________");
        Menu.getInput(1, 1);
        System.out.println("Thats good idea! Lets choose the mode : \n1.Generate random things behind the doors\n2)Input by keyboard");
        if (Menu.getInput(1, 2) == 1) {
            mode = true;
        } else {
            mode = false;
        }
        boolean isPlaying = true;
        Battlefield battlefield;
        do {
            battlefield = new Battlefield(mode);
            battlefield.play();
            System.out.println("\n\n\nWanna play 1 more time ? 1-yes , 2 - no");
            if (Menu.getInput(1, 2) == 2) {
                isPlaying = false;
            }
        } while (isPlaying);

    }
}
