package com.epam.trainings.logicaltasksforarrays.artifactgametask;

import com.epam.trainings.logicaltasksforarrays.artifactgametask.utils.Const;

public class Player {

    private int force;
    private boolean won;


    Player() {
        this.force = 25;
    }

    public Player(int force, boolean won) {
        this.force = force;
        this.won = won;
    }

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public boolean playerChangeForce(Door door) {
        int playerForce = this.force + door.changedForce;
        if (playerForce >= 0) {
            door.setForceBeforeAndAfter(this.force, playerForce);
            this.force = playerForce;
            return Const.PLAYER_STILL_ALIVE;
        } else {
            return Const.PLAYER_IS_DEAD;
        }
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
