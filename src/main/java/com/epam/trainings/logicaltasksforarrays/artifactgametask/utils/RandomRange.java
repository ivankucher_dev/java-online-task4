package com.epam.trainings.logicaltasksforarrays.artifactgametask.utils;

import com.epam.trainings.logicaltasksforarrays.artifactgametask.Door;
import java.util.ArrayList;
import java.util.Random;

public class RandomRange {


    public static Door getRandomBehindDoor() {
        int changedForce;
        Random r = new Random();
        if (r.nextInt(2) == 0) {
            changedForce = -(r.nextInt((Const.MAX_DAMAGE_FOR_ENEMY - Const.MIN_DAMAGE_FOR_ENEMY) + 1) + Const.MIN_DAMAGE_FOR_ENEMY);
            return (new Door(Const.ENEMY, changedForce));
        } else {
            changedForce = r.nextInt((Const.MAX_INCREASE_BY_ARTIFACT - Const.MIN_INCREASE_BY_ARTIFACT) + 1) + Const.MIN_INCREASE_BY_ARTIFACT;
            return (new Door(Const.ARTIFACT, changedForce));
        }

    }

    public static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static int getRandomDoorForAlgorythm(ArrayList<Integer> wayList, int playerForce, ArrayList<Door> doors) {
        int max = 9, min = 0;
        int doorNumber;
        Random r = new Random();
        do {
            doorNumber = r.nextInt((max - min) + 1) + min;
            if (!wayList.contains(doorNumber) && (playerForce + doors.get(doorNumber).getChangedForce()) >= 0 && doors.get(doorNumber).isClosed()) {
                return doorNumber;
            }
        } while (true);

    }
}
