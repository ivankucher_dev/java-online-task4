package com.epam.trainings.logicaltasksforarrays.artifactgametask;

import com.epam.trainings.collectionstasks.secondtask.Menu;
import com.epam.trainings.logicaltasksforarrays.artifactgametask.utils.Const;
import com.epam.trainings.logicaltasksforarrays.artifactgametask.utils.RandomRange;
import com.epam.trainings.logicaltasksforarrays.artifactgametask.utils.TableOutput;
import com.epam.trainings.logicaltasksforarrays.artifactgametask.utils.Utils;
import java.util.ArrayList;
import java.util.Scanner;

public class Battlefield {


    Player player;
    ArrayList<Door> doors;
    Scanner sc;

    Battlefield(boolean generateEnemiesAutomatically) {
        sc = new Scanner(System.in);
        if (!generateEnemiesAutomatically) {
            doors = new ArrayList<Door>();
            player = new Player();
            initializeDoorsInput();
        } else {
            doors = new ArrayList<Door>();
            player = new Player();
            initializeRandomDoors();
        }
    }

    private void initializeRandomDoors() {
        System.out.println("\n\t# Automatically generated doors data #\n");
        for (int i = 0; i < Const.DOORS_AMOUNT; i++) {
            doors.add(RandomRange.getRandomBehindDoor());
        }
        TableOutput.showGameInfo(doors);
    }

    private void initializeDoorsInput() {
        System.out.println("#################################");
        System.out.println("FILING DOORS\n");
        System.out.println("Enter 'e' to set enemy behind the door OR 'a' to set artifact");
        for (int i = 0; i < Const.DOORS_AMOUNT; i++) {
            String enemyOrArtifact;
            int changedPower;
            do {
                System.out.print("Behind the door №" + (i) + " : ");
                enemyOrArtifact = Utils.recognize(sc.nextLine());
                System.out.print("Power : ");
                changedPower = sc.nextInt();
                sc.nextLine();
                if (enemyOrArtifact.equals(Const.ENEMY)) {
                    changedPower = -(changedPower);
                }
            } while (!Utils.checkForNomralRangeInput(enemyOrArtifact, changedPower));
            doors.add(new Door(enemyOrArtifact.trim().toLowerCase(), changedPower));
        }
        TableOutput.showGameInfo(doors);
    }

    public Player play() {
        boolean openRandomly;
        boolean isWon = true;
        int numberOfPossibleDeath;
        for (int i = 0; i < Const.DOORS_AMOUNT; i++) {
            if (i > 0) {
                TableOutput.gameProcessInfo(player, doors);
            }
            System.out.println("Do you want to show the possible way to win the game? 1-yes ,0-no");
            if (Menu.getInput(0, 1) == 1) {
                algorithmToWin();
            }
            System.out.println("Player force avaliable : " + player.getForce());
            numberOfPossibleDeath = numberOfYourExcpectedDeath(Const.DOORS_AMOUNT);
            System.out.println("Possible death behind the : " + numberOfPossibleDeath + " doors");
            System.out.println("Do you wanna shows id of this doors? 1-yes , 0-no");
            if (Menu.getInput(0, 1) == 1) {
                ArrayList<Integer> doorsId = new ArrayList<>();
                doorsIdOfYourExcpectedDeath(Const.DOORS_AMOUNT, doorsId);
            }
            System.out.println("Do you want to open door randomly ? 1-yes , 0-no");
            openRandomly = "1".equals(String.valueOf(Menu.getInput(0, 1)));
            isWon = player.playerChangeForce(doors.get(openDoor(openRandomly)));
            if (!isWon) {
                System.out.println("!!!!!!!!!!!!!!  :(   OH SORRY , U LOSE   :(   !!!!!!!!!!!!!");
                endGame();
                return player;
            }

        }
        System.out.println("*******CONGRATULATIONS, YOU WIN**********");
        endGame();
        return player;
    }


    public int openDoor(boolean openRandomly) {
        int doorNumber;
        int i = 0;
        if (openRandomly) {
            do {
                doorNumber = RandomRange.getRandomNumberInRange(0, 9);
            } while (!doors.get(doorNumber).isClosed());
            System.out.println("Opening door number " + doorNumber + "....");
            doors.get(doorNumber).setClosed(false);
            return doorNumber;
        } else {
            System.out.println("Avaliable doors to open : ");
            TableOutput.showGameInfo(doors);
            do {
                System.out.print("Enter door number to open : ");
                doorNumber = sc.nextInt();
            } while (!doors.get(doorNumber).isClosed());
            doors.get(doorNumber).setClosed(false);
            return doorNumber;
        }
    }


    public void algorithmToWin() {
        Player player1 = new Player(player.getForce(), player.isWon());
        boolean winPossibility = checkForWinPossibility();
        int avaliableDoors = Utils.getAvaliableDoorsToOpen(doors);
        ArrayList<Integer> waysList = new ArrayList<>();
        boolean isWin = false;
        if (winPossibility) {
            while (isWin == false) {
                for (int j = 0; j < avaliableDoors; j++) {
                    int doorNumber = RandomRange.getRandomDoorForAlgorythm(waysList, player1.getForce(), doors);
                    waysList.add(doorNumber);
                    player1.playerChangeForce(doors.get(doorNumber));
                    if (j == (avaliableDoors - 1)) {
                        if (player1.getForce() >= 0) {
                            isWin = true;
                        }
                    }
                }
            }
            System.out.println("\n*******************WAY TO WIN THE GAME*******************\n");
            for (int k : waysList) {
                System.out.print(k + " ");
            }
            System.out.println("\nEnd player force : " + player1.getForce());
        } else {
            System.out.println("Win is impossible");
            System.exit(1);
        }

    }

    public boolean checkForWinPossibility() {
        int totalPlayerForce = 25;
        int totalEnemyForce = 0;
        for (Door door : doors) {
            if (door.getEnemyOrArtifact().equals(Const.ENEMY) && door.isClosed()) {
                totalEnemyForce += door.getChangedForce();
            } else {
                totalPlayerForce += door.getChangedForce();
            }

        }
        if (Math.abs(totalEnemyForce) > totalPlayerForce) {
            return false;
        } else {
            return true;
        }
    }


    public int numberOfYourExcpectedDeath(int doorsAmount) {
        if (doorsAmount == 0) {
            return 0;
        }
        if (doors.get(doorsAmount - 1).isClosed() && doors.get(doorsAmount - 1).getEnemyOrArtifact().equals(Const.ENEMY)) {
            if (Math.abs(doors.get(doorsAmount - 1).getChangedForce()) > player.getForce()) {
                return (1 + numberOfYourExcpectedDeath(doorsAmount - 1));
            } else {
                return numberOfYourExcpectedDeath(doorsAmount - 1);
            }
        } else {
            return numberOfYourExcpectedDeath(doorsAmount - 1);
        }


    }


    public int doorsIdOfYourExcpectedDeath(int doorsNumber, ArrayList<Integer> doorsId) {
        if (doorsNumber > Const.DOORS_AMOUNT) {
            throw new IllegalArgumentException("Doors amount must be " + Const.DOORS_AMOUNT);
        }
        if (doorsNumber == 0) {
            System.out.println("Doors number : ");
            for (int i : doorsId) {
                System.out.print(i + " ");
            }
            System.out.println("");
            return 0;
        }
        if (doors.get(doorsNumber - 1).isClosed() && doors.get(doorsNumber - 1).getEnemyOrArtifact().equals(Const.ENEMY)) {
            if (Math.abs(doors.get(doorsNumber - 1).getChangedForce()) > player.getForce()) {
                doorsId.add(doorsNumber - 1);
                return (1 + doorsIdOfYourExcpectedDeath(doorsNumber - 1, doorsId));
            } else {
                return doorsIdOfYourExcpectedDeath(doorsNumber - 1, doorsId);
            }
        } else {
            return doorsIdOfYourExcpectedDeath(doorsNumber - 1, doorsId);
        }


    }

    public void endGame() {
        doors = new ArrayList<Door>();
        player = new Player();
    }
}
