package com.epam.trainings.logicaltasksforarrays.artifactgametask.utils;

import com.epam.trainings.logicaltasksforarrays.artifactgametask.Door;

import java.util.ArrayList;

public class Utils {

    public static boolean checkForNomralRangeInput(String enemyOrArtifact, int changedPower) {
        if (enemyOrArtifact.equals(Const.ENEMY)) {
            if (changedPower >= -100 && changedPower <= -5) {
                return true;
            }
            return false;
        } else {
            if (changedPower <= 80 && changedPower >= 10) {
                return true;
            }
            return false;
        }

    }

    public static int getAvaliableDoorsToOpen(ArrayList<Door> doors) {
        int count = 0;
        for (Door door : doors) {
            if (door.isClosed()) {
                count++;
            }
        }
        return count;
    }

    public static String recognize(String enemyOrArtifact) {
        if (enemyOrArtifact.equals("e")) {
            return Const.ENEMY;
        } else if (enemyOrArtifact.equals("a")) {
            return Const.ARTIFACT;
        }
        return "illegal";
    }
}
