package com.epam.trainings.logicaltasksforarrays.artifactgametask.utils;

import com.epam.trainings.logicaltasksforarrays.artifactgametask.Door;
import com.epam.trainings.logicaltasksforarrays.artifactgametask.Player;

import javax.imageio.ImageTranscoder;
import java.util.ArrayList;
import java.util.HashMap;

public class TableOutput {

    private static String symbol;


    public static void showGameInfo(ArrayList<Door> doors) {
        int i = -1;
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.printf("%10s %30s %20s", "DOOR ID", "BEHIND THE DOOR", "POWER");
        System.out.println();
        System.out.println("------------------------------------------------------------------------------------------------------------");
        for (Door door : doors) {
            i++;
            if (door.isClosed()) {
                if (door.getEnemyOrArtifact().equals(Const.ENEMY)) {
                    symbol = "-";
                } else {
                    symbol = "+";
                }
                System.out.format("%5d %30s %21s %1s",
                        i, door.getEnemyOrArtifact(), symbol, Math.abs(door.getChangedForce()));
                System.out.println();
            }
        }
        System.out.println("------------------------------------------------------------------------------------------------------------");
    }


    public static void gameProcessInfo(Player player, ArrayList<Door> doors) {
        int i = -1;
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.printf("%15s %30s %20s %20s", "NUMBER OF OPENED DOOR", "WHAT WAS BEHIND THE DOOR", "FORCE BEFORE", "FORCE AFTER");
        System.out.println();
        System.out.println("------------------------------------------------------------------------------------------------------------");
        for (Door door : doors) {
            i++;
            if (!door.isClosed()) {
                if (door.getEnemyOrArtifact().equals(Const.ENEMY)) {
                    symbol = "-";
                } else {
                    symbol = "+";
                }
                System.out.format("%8d %35s %20s %20s ",
                        i, (door.getEnemyOrArtifact() + "\t" + symbol + Math.abs(door.getChangedForce())),
                        door.getForceBefore(), door.getForceAfter());
                System.out.println();
            }
        }
        System.out.println("------------------------------------------------------------------------------------------------------------");
    }


}


