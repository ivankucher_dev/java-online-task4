package com.epam.trainings.logicaltasksforarrays.artifactgametask;


public class Door {

    String enemyOrArtifact;
    int changedForce;
    boolean closed;
    int forceBefore;
    int forceAfter;

    public Door(String enemyOrArtifact, int changedForce) {
        if (!enemyOrArtifact.toLowerCase().equals("enemy") && !enemyOrArtifact.toLowerCase().equals("artifact")) {
            throw new IllegalArgumentException("Behind the door must be just 'enemy' OR 'artifact'");
        } else {
            this.enemyOrArtifact = enemyOrArtifact;
            this.changedForce = changedForce;
            this.closed = true;
        }
    }

    public void setForceBeforeAndAfter(int forceBefore, int forceAfter) {
        this.forceBefore = forceBefore;
        this.forceAfter = forceAfter;
    }

    public int getForceAfter() {
        return forceAfter;
    }

    public int getForceBefore() {
        return forceBefore;
    }

    public String getEnemyOrArtifact() {
        return enemyOrArtifact;
    }


    public int getChangedForce() {
        return changedForce;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
