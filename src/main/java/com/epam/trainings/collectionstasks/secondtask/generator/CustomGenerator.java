package com.epam.trainings.collectionstasks.secondtask.generator;

import com.epam.trainings.collectionstasks.secondtask.StringsObject;

import java.util.*;

public class CustomGenerator<T> {

    public static final Random gen = new Random();
    public static int amountOfCountries;
    Set<Integer> used = new HashSet<>();
    Map<String, String> pairs;



    public CustomGenerator() {
        CountryCapitalPairs c = new CountryCapitalPairs();
        c.initializePairs();
        pairs = c.getCountryCapitalPairs();
        amountOfCountries = pairs.size();
    }

    public T getRandomCountries(int amount, T obj) {
        if(amount>amountOfCountries){
            throw new IllegalArgumentException("amount must be lower than "+amountOfCountries);
        }
        ArrayList<StringsObject> list = new ArrayList<>();
        StringsObject [] array= new StringsObject[amount];
        Object[] countries = pairs.keySet().toArray();
        if(obj instanceof ArrayList<?>) {
            for (int i = 0; i < amount; i++) {
                String country = countries[getUniqueRandomNumber(countries.length - 1)].toString();
                String capital = pairs.get(country);
                list.add(new StringsObject(country,capital));
                obj = (T) list;
            }
        }
        else if(obj.getClass().isArray()){
            for (int i = 0; i < amount; i++) {
                String country = countries[getUniqueRandomNumber(countries.length - 1)].toString();
                String capital = pairs.get(country);
                array[i] = (new StringsObject(country,capital));
            }
            obj = (T) array;
        }
        used.clear();
        return obj;
    }

    private int getUniqueRandomNumber(int maxRange) {
        int result;
        do {
            result = gen.nextInt(maxRange + 1);
        } while (used.contains(result));
        used.add(result);

        return result;
    }
}
