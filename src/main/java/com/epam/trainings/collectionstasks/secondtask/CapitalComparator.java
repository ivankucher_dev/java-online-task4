package com.epam.trainings.collectionstasks.secondtask;

import java.util.Comparator;

public class CapitalComparator implements Comparator<StringsObject> {
    @Override
    public int compare(StringsObject o1, StringsObject o2) {
        return o1.capital.compareTo(o2.capital);
    }


}
