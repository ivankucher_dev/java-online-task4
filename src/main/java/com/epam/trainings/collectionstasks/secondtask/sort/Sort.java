package com.epam.trainings.collectionstasks.secondtask.sort;

import com.epam.trainings.collectionstasks.secondtask.CapitalComparator;
import com.epam.trainings.collectionstasks.secondtask.StringsObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Sort {

    public static void sortByComparator(StringsObject[] array , ArrayList<StringsObject> list){
        CapitalComparator comparator = new CapitalComparator();
        Arrays.sort(array, comparator);
        Collections.sort(list,comparator);
    }

    public static void sortByComparable(StringsObject[] array , ArrayList<StringsObject> list){
        Arrays.sort(array);
        Collections.sort(list);
    }
}
