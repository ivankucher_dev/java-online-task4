package com.epam.trainings.collectionstasks.secondtask.print;

import com.epam.trainings.collectionstasks.secondtask.StringsObject;

import java.util.ArrayList;

public class Printer {

    public static void printSortedByCapital(ArrayList<StringsObject> list , StringsObject [] array){
        System.out.println("---------------COMPARATOR SORT----------------");
        System.out.println("---------------Sorted by capital----------------");
        System.out.println("\n************ARRAY LIST************");
        for(StringsObject o : list){
            System.out.println("Capital : "+o.getCap() + "      |        Country : "+ o.getCountry());
        }
        System.out.println("\n************ARRAY************");
        for(StringsObject o : array){
            System.out.println("Capital : "+o.getCap() + "      |        Country : "+ o.getCountry());
        }
    }

    public static void printSortedByCountry(ArrayList<StringsObject> arrayList , StringsObject [] arrayObjects){
        System.out.println("---------------COMPARABLE SORT----------------");
        System.out.println("---------------Sorted by country----------------");
        System.out.println("\n************ARRAY LIST************");

        for(StringsObject o : arrayList){
            System.out.println("Country : "+o.getCountry() + "    |     Capital : "+ o.getCap());
        }
        System.out.println("\n************ARRAY************");
        for(StringsObject o : arrayObjects){
            System.out.println("Country : "+o.getCountry() + "    |     Capital : "+ o.getCap());
        }
    }

    public static void printMainMenu(){
        System.out.println("\nEpam Trainings 2019");
        System.out.println("Kucher Ivan");
        System.out.println("\nMain menu : ");
        System.out.println("1.Sort by Comparator ( by capital )");
        System.out.println("2.Sort by Comparable ( by country )");
        System.out.println("3.Binary search perform :");
        System.out.println("4.Exit");


    }

}
