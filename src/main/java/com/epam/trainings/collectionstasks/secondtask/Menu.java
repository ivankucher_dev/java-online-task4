package com.epam.trainings.collectionstasks.secondtask;

import com.epam.trainings.collectionstasks.secondtask.generator.CustomGenerator;
import com.epam.trainings.collectionstasks.secondtask.print.Printer;
import com.epam.trainings.collectionstasks.secondtask.sort.Sort;

import java.util.*;

public class Menu {

    CustomGenerator<StringsObject[]> arrayGen;
    CustomGenerator<ArrayList<StringsObject>> listGen;

    public Menu() {
        arrayGen = new CustomGenerator<>();
        listGen = new CustomGenerator<>();
    }

    public static int getInput(int firstLimit, int secondLimit) {
        Scanner scanner = new Scanner(System.in);
        int choise = -1;
        while (choise < firstLimit || choise > secondLimit) {
            try {
                System.out.println("Your choise : ");
                choise = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Input number must be like in menu!");
                scanner.next();
                continue;
            }
        }

        return choise;
    }


    public void multiLevelMenu() {

        int input;
        int amount;
        StringsObject[] array = new StringsObject[10];
        ArrayList<StringsObject> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the amount of countries <from 1 to 19>");
        amount = sc.nextInt();

        do {
            array = arrayGen.getRandomCountries(amount, array);
            list = listGen.getRandomCountries(amount, list);
            Printer.printMainMenu();
            input = Menu.getInput(1, 4);
            switch (input) {
                case 1:
                    System.out.println("\n");
                    Sort.sortByComparator(array, list);
                    Printer.printSortedByCapital(list, array);
                    break;


                case 2:
                    Sort.sortByComparable(array, list);
                    Printer.printSortedByCountry(list, array);
                    break;

                case 3:
                    CapitalComparator comparator = new CapitalComparator();

                    System.out.println("Enter country and capital to search separeted by space : ");
                    sc.next();
                    String[] result = sc.nextLine().split(" ");
                    String country = result[0];
                    String capital = result[1];
                    Sort.sortByComparator(array, list);
                    int arrayIndex = Arrays.binarySearch(array, new StringsObject(country, capital), comparator);
                    int listIndex = Collections.binarySearch(list, new StringsObject(country, capital), comparator);

                    try {
                        System.out.println("Found in array : ");
                        System.out.println("Capital : " + array[arrayIndex].capital + "|| Country : " + array[arrayIndex].country);
                        System.out.println("");
                        System.out.println("Found in list : ");
                        System.out.println("Capital : " + list.get(listIndex).capital + "|| Country : " + list.get(listIndex).country);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println("Not found");
                    }
                    break;


                case 4:
                    //вихід з програми
                    System.exit(1);
                    break;

            }
        } while (input != 4);
    }

}
