package com.epam.trainings.collectionstasks.secondtask;

public class StringsObject implements Comparable<StringsObject> {

    String country;
    String capital;

    @Override
    public int compareTo(StringsObject o) {
        return this.country.compareTo(o.country);
    }

    public StringsObject(String country, String capital){
        this.country = country;
        this.capital = capital;
    }

    public String getCountry(){
        return country;
    }
    public String getCap(){
        return capital;
    }

}
