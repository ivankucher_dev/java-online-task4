package com.epam.trainings.collectionstasks.secondtask.generator;


import java.util.HashMap;
import java.util.Map;

public class CountryCapitalPairs {

    private Map<String, String> countryCapitalPairs;

CountryCapitalPairs(){
    countryCapitalPairs = new HashMap<>();
}
    public void initializePairs() {
        addCountry("Austria", "Vienna");
        addCountry("Belgium", "Brussels");
        addCountry("Cyprus", "Nicosia");
        addCountry("Estonia", "Tallinn");
        addCountry("Finland", "Helsinki");
        addCountry("France", "Paris");
        addCountry("Germany", "Berlin");
        addCountry("Greece", "Athens");
        addCountry("Ireland", "Dublin");
        addCountry("Italy", "Rome");
        addCountry("Luxembourg", "Luxembourg");
        addCountry("Malta", "Malletta");
        addCountry("Netherlands", "Amsterdam");
        addCountry("Portugal", "Lisbon");
        addCountry("Slovakia", "Bratislava");
        addCountry("Slovenia", "Ljubljiana");
        addCountry("Spain", "Madrid");
        addCountry("Ukraine", "Kyiv");
        addCountry("Russia", "Moskow");
    }

    private void addCountry(String country, String capital) {
        countryCapitalPairs.put(country,capital);
    }

    public Map<String, String> getCountryCapitalPairs() {
        return countryCapitalPairs;
    }
}
