package com.epam.trainings.collectionstasks.containertask;

import java.util.Arrays;

public class StringContainer {

    private String[] arrayOfStrings;
    private int size;

    public String[] getStrings() {
        return Arrays.copyOf(arrayOfStrings, arrayOfStrings.length);

    }

    public StringContainer() {
        size = 0;
        arrayOfStrings = new String[size];

    }

    public void addString(String string) {
        arrayOfStrings = Arrays.copyOf(arrayOfStrings, ++size);
        arrayOfStrings[size - 1] = string;
    }
}
