package com.epam.trainings.collectionstasks.containertask.main;

import com.epam.trainings.collectionstasks.containertask.PerformanceComparator;
import com.epam.trainings.collectionstasks.containertask.StringContainer;

public class ContainerMain {
    public static void main(String[] args) {
        StringContainer stringContainer = new StringContainer();
        String performanceInfo = PerformanceComparator.comparePerformance();
        System.out.println(performanceInfo);
    }
}
