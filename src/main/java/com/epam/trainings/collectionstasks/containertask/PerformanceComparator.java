package com.epam.trainings.collectionstasks.containertask;

import java.util.ArrayList;

public class PerformanceComparator {

    private static ArrayList<String> o1;
    private static StringContainer o2;
    private static final int CONST_ELEMENTS_TO_ADD = 100;

    public static String comparePerformance() {
        String[] stringsToAdd = new String[CONST_ELEMENTS_TO_ADD];
        o1 = new ArrayList<>();
        o2 = new StringContainer();
        long startTime;
        long endTime;
        long resultTimeO1;
        long resultTimeO2;
        stringsToAddInitializer(stringsToAdd);

        startTime = System.currentTimeMillis();
        for (String k : stringsToAdd) {
            o2.addString(k);
        }
        endTime = System.currentTimeMillis();
        resultTimeO2 = endTime - startTime;

        startTime = System.currentTimeMillis();
        for (String k : stringsToAdd) {
            o1.add(k);
        }
        endTime = System.currentTimeMillis();
        resultTimeO1 = endTime - startTime;

        if (resultTimeO1 < resultTimeO2) {
            return "ArrayList faster by " + (resultTimeO2 - resultTimeO1) + " millis";
        } else {
            return "String Container faster by " + (resultTimeO1 - resultTimeO2) + " millis";
        }

    }

    private static void stringsToAddInitializer(String[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = i + " string";
        }
    }


}
